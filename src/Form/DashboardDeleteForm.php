<?php

namespace Drupal\dashboard\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds a form to delete a dashboard.
 *
 * @internal
 */
class DashboardDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the dashboard %name?', [
      '%name' => $this->entity->label(),
    ]);
  }

}
