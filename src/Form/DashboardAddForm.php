<?php

namespace Drupal\dashboard\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for adding a dashboard.
 *
 * @internal
 */
class DashboardAddForm extends DashboardFormBase {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = t('Add dashboard');
    return $actions;
  }

}
