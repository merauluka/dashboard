<?php

namespace Drupal\dashboard\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for editing a dashboard.
 *
 * @internal
 */
class DashboardEditForm extends DashboardFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['path']['#default_value'] = $this->entity->getPath();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = t('Save dashboard');
    unset($actions['delete']);
    return $actions;
  }

}
