<?php

namespace Drupal\dashboard\Form;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base form for dashboards.
 */
abstract class DashboardFormBase extends EntityForm {

  use StringTranslationTrait;

  /**
   * The dashboard storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $dashboardStorage;

  /**
   * Constructs a new dashboard form.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $dashboard_storage
   *   The dashboard storage.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   */
  public function __construct(ConfigEntityStorageInterface $dashboard_storage, RouteBuilderInterface $router_builder) {
    $this->dashboardStorage = $dashboard_storage;
    $this->routerBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('dashboard'),
      $container->get('router.builder')
    );
  }

  /**
   * Checks for an existing dashboard.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element) {
    return (bool) $this->dashboardStorage
      ->getQuery()
      ->condition('id', $entity_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => 'Name',
      '#maxlength' => 100,
      '#description' => $this->t('Name of the dashboard'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#description' => $this->t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)|(^custom$)',
        'error' => $this->t('The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores. Additionally, it can not be the reserved word "custom".'),
      ],
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getDescription(),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#maxlength' => 255,
      '#description' => $this->t('The path to access this dashboard. Should only include URL safe characters.'),
      '#default_value' => $this->entity->getPath(),
      '#required' => TRUE,
    ];

    $form['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#description' => $this->t('The layout to use for this dashboard.'),
      '#default_value' => $this->entity->getLayout(),
      '#options' => ['test'],
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getStatus(),
    ];

    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Language'),
      '#languages' => LanguageInterface::STATE_ALL,
      '#default_value' => $this->entity->language()->getId(),
    ];
    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $path = $values['path'];
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('description', trim($form_state->getValue('description')));
    $form_state->setValue('path', trim($form_state->getValue('path')));
    $form_state->setValue('layout', trim($form_state->getValue('layout')));
    $form_state->setValue('status', trim($form_state->getValue('status')));
    // Rebuild the router cache when the dashboard is saved.
    $this->routerBuilder->rebuild();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Custom dashboard updated.'));
    }
    else {
      $this->messenger()->addStatus($this->t('Custom dashboard added.'));
    }
    $form_state->setRedirectUrl($this->entity->urlInfo('collection'));
  }

}
