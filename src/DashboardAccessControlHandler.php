<?php

namespace Drupal\dashboard;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the dashboard entity type.
 *
 * @see \Drupal\dashboard\Entity\Dashboard
 */
class DashboardAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // There are no restrictions on viewing the label of a dashboard.
    if ($operation === 'view label') {
      return AccessResult::allowed();
    }
    // Locked dashboards cannot be updated or deleted.
    elseif (in_array($operation, ['update', 'delete'])) {
      if ($entity->isLocked()) {
        return AccessResult::forbidden('The Dashboard config entity is locked.')->addCacheableDependency($entity);
      }
      else {
        return parent::checkAccess($entity, $operation, $account)->addCacheableDependency($entity);
      }
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
