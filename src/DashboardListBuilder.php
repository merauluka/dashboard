<?php

namespace Drupal\dashboard;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of dashboard entities.
 *
 * @see \Drupal\dashboard\Entity\Dashboard
 */
class DashboardListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = t('Name');
    $header['path'] = t('Path');
    $header['description'] = t('Description');
    $header['status'] = t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['path'] = $entity->getPath();
    $row['description'] = $entity->getDescription();
    $row['status'] = empty($entity->getStatus()) ? t('No') : t('Yes');
    return $row + parent::buildRow($entity);
  }

}
