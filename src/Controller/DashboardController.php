<?php

namespace Drupal\dashboard\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for dashboard module routes.
 */
class DashboardController extends ControllerBase {

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The url generator service.
   *
   * @var \Drupal\Core\Render\MetadataBubblingUrlGenerator
   */
  protected $urlGenerator;

  /**
   * DashboardController constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   * @param \Drupal\Core\Render\MetadataBubblingUrlGenerator $url_generator
   *   The url generator service.
   */
  public function __construct(AccountProxyInterface $current_user, MetadataBubblingUrlGenerator $url_generator) {
    $this->currentUser = $current_user;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('url_generator')
    );
  }

  /**
   * Clears all caches, then redirects to the previous page.
   */
  public function adminPage() {
    $js_settings = [
      'dashboard' => [
        'drawer' => $this->urlGenerator->generateFromRoute('dashboard.admin.drawer'),
        'blockContent' => $this->urlGenerator->generateFromRoute('dashboard.admin.block_content'),
        'updatePath' => $this->urlGenerator->generateFromRoute('dashboard.admin.update'),
        // 'formToken' => drupal_get_token('dashboard-update'),
        // 'launchCustomize' => $launch_customize,
        'dashboard' => $this->urlGenerator->generateFromRoute('dashboard.admin'),
        'emptyBlockText' => $this->t('(empty)'),
        'emptyRegionTextInactive' => $this->t('This dashboard region is empty. Click <em>Customize dashboard</em> to add blocks to it.'),
        'emptyRegionTextActive' => $this->t('DRAG HERE'),
      ],
    ];
    // @TODO: Update @dashboard to point to the configure page.
    $build = [
      '#theme' => 'dashboard_admin',
      '#message' => $this->t('To customize the dashboard page, move blocks to the dashboard regions on the <a href="@dashboard">Dashboard administration page</a>, or enable JavaScript on this page to use the drag-and-drop interface.', [
        '@dashboard' => $this->urlGenerator->generateFromRoute('dashboard.admin'),
      ]),
      '#access' => $this->currentUser->hasPermission('administer blocks'),
      '#attached' => [
        'drupalSettings' => [
          'dashboard' => $js_settings,
        ],
        'library' => [
          'dashboard/dashboard',
        ],
      ],
    ];
    return $build;
  }

}
