<?php

namespace Drupal\dashboard;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a dashboard.
 */
interface DashboardInterface extends ConfigEntityInterface {

  /**
   * Gets the description string for this dashboard.
   *
   * @return string
   *   The description for the dashboard.
   */
  public function getDescription();

  /**
   * Sets the description for this dashboard.
   *
   * @param string $description
   *   The description to use for this dashboard.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the layout value for this dashboard.
   *
   * @return string
   *   The layout for the dashboard.
   */
  public function getLayout();

  /**
   * Sets the layout for this dashboard.
   *
   * @param string $layout
   *   The layout to use for this dashboard.
   *
   * @return $this
   */
  public function setLayout($layout);

  /**
   * Gets the path value for this dashboard.
   *
   * @return string
   *   The path for the dashboard.
   */
  public function getPath();

  /**
   * Sets the path for this dashboard.
   *
   * @param string $path
   *   The path to use for this dashboard.
   *
   * @return $this
   */
  public function setPath($path);

  /**
   * Gets the status value for this dashboard.
   *
   * @return string
   *   The status for the dashboard.
   */
  public function getStatus();

  /**
   * Sets the status for this dashboard.
   *
   * @param bool $status
   *   The status to use for this dashboard.
   *
   * @return $this
   */
  public function setStatus($status);

  /**
   * Determines if this dashboard is locked.
   *
   * @return bool
   *   TRUE if the dashboard is locked, FALSE otherwise.
   */
  public function isLocked();

}
