<?php

namespace Drupal\dashboard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\dashboard\DashboardInterface;

/**
 * Defines the Dashboard type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "dashboard",
 *   label = @Translation("Dashboard"),
 *   handlers = {
 *     "access" = "Drupal\dashboard\DashboardAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\dashboard\Form\DashboardAddForm",
 *       "edit" = "Drupal\dashboard\Form\DashboardEditForm",
 *       "delete" = "Drupal\dashboard\Form\DashboardDeleteForm"
 *     },
 *     "list_builder" = "Drupal\dashboard\DashboardListBuilder",
 *   },
 *   admin_permission = "administer dashboards",
 *   config_prefix = "config",
 *   static_cache = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "canonical" = "/dashboard/{dashboard}",
 *     "add-form" = "/admin/structure/dashboard/add",
 *     "edit-form" = "/admin/structure/dashboard/manage/{dashboard}",
 *     "delete-form" = "/admin/structure/dashboard/manage/{dashboard}/delete",
 *     "collection" = "/admin/structure/dashboard",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "layout",
 *     "path",
 *     "active"
 *   }
 * )
 */
class Dashboard extends ConfigEntityBase implements DashboardInterface {

  /**
   * The machine name of this dashboard.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the dashboard.
   *
   * @var string
   */
  protected $label;

  /**
   * The layout of the dashboard.
   *
   * @var string
   */
  protected $layout;

  /**
   * A brief description of this dashboard.
   *
   * @var string
   */
  protected $description;

  /**
   * The locked status of this date format.
   *
   * @var bool
   */
  protected $locked = FALSE;

  /**
   * A custom url path for this dashboard.
   *
   * @var string
   */
  protected $path;

  /**
   * Whether or not this dashboard is actively in use.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    return (bool) $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setLayout($layout) {
    $this->layout = $layout;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayout() {
    return $this->layout;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    if ($a->isLocked() == $b->isLocked()) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return $a->isLocked() ? 1 : -1;
  }

}
